package com.ciriscr.dbf2csv

import java.io.File
import com.healthmarketscience.jackcess.DatabaseBuilder
import com.healthmarketscience.jackcess.util.ExportUtil.Builder

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/13
 * Time: 11:29 AM
 */

class AccessReader(inputFile: String, outputFile: String, withHeader: Boolean, splitter: String) {

  def exportar() {
    val outpot = new File(outputFile)
    require(outpot.isDirectory)
    val bd = DatabaseBuilder.open(new File(inputFile))
    new Builder(bd).setDelimiter(splitter).setHeader(withHeader).exportAll(new File(outputFile))
    bd.close()
  }
}
