//Nombre del proyecto
name := "access2csv"

version := "0.1"

organization := "com.ciriscr.access2csv"

scalaVersion := "2.11.2"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases"

seq(assemblySettings: _*)

//Casbah, Scalatest, akka, salat
libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.0" % "test",
  "com.healthmarketscience.jackcess" % "jackcess" % "2.0.4",
  "joda-time" % "joda-time" % "2.3"
)

// para compilar en varias versiones de scala
crossScalaVersions := Seq("2.9.2", "2.10.0", "2.10.2")

// reduce the maximum number of errors shown by the Scala compiler
maxErrors := 30

// increase the time between polling for file changes when using continuous execution
pollInterval := 1000

// append several options to the list of options passed to the Java compiler
javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

// append -deprecation to the options passed to the Scala compiler
scalacOptions ++= Seq("-deprecation", "-optimise", "-explaintypes")


// set the main class for packaging the main jar
// 'run' will still auto-detect and prompt
// change Compile to Test to set it for the test jar
//mainClass in (Compile, packageBin) := Some("myproject.MyMain")

// set the main class for the main 'run' task
// change Compile to Test to set it for 'test:run'
//mainClass in (Compile, run) := Some("myproject.MyMain")


// only use a single thread for building
parallelExecution := true
